//
//  ViewController.swift
//  slideViewTutorial
//
//  Created by ProObject on 7/19/17.
//  Copyright © 2017 ProObject. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var masterView: UIView!
    @IBOutlet weak var slideView: TrayView!
    @IBOutlet var gestureRecognizer: UIPanGestureRecognizer!
    
    @IBOutlet weak var point1Button: UIButton!
    @IBOutlet weak var point2Button: UIButton!
    
    var trayIsHidden = true
    var trayNumBeingShown = 0
    var isShowPanel: Bool!
    var isDown: Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        slideView.parentView = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        isShowPanel = false
        isDown = true
        self.setTrayConstraintsToDown()
        self.slideView.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        slideView.translatesAutoresizingMaskIntoConstraints = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func point1Listener(_ sender: Any) {
        toggleTrayHidden(sender: 1)
    }
    
    @IBAction func point2Listener(_ sender: Any) {
        toggleTrayHidden(sender: 2)
    }
    
    func toggleTrayHidden(sender: Int) {
        slideView.isHidden = true
        if trayNumBeingShown == sender {
            trayIsHidden = true
            trayNumBeingShown = 0
        } else {
            trayIsHidden = false
            trayNumBeingShown = sender
        }
        slideView.useData(fromPoint: sender)
        self.setTrayConstraintsToDown()
        self.slideView.isHidden = trayIsHidden
    }
    
    func pullUp() {
        masterView.bringSubview(toFront: slideView)
        self.isDown = false
        UIView.animate(withDuration: 0.25, animations: {
            self.setTrayConstraintsToUp()
        })
    }
    
    func pullDown() {
        masterView.bringSubview(toFront: slideView)
        self.isDown = true
        UIView.animate(withDuration: 0.25, animations: {
            self.setTrayConstraintsToDown()
        })
    }
    
    func setTrayConstraintsToUp() {
        self.slideView.frame.origin.y = 20
    }
    
    func setTrayConstraintsToDown() {
        self.slideView.frame.origin.y = self.masterView.frame.origin.y + self.masterView.frame.size.height - 97
    }

    @IBAction func handlePan(_ sender: Any) {
        masterView.bringSubview(toFront: slideView)
        let casted: CGFloat = 0
        (sender as? UITapGestureRecognizer)?.view?.layer.removeAllAnimations()
        let translatedPoint: CGPoint? = (sender as? UIPanGestureRecognizer)?.translation(in: view)
        let velocity: CGPoint? = (sender as? UIPanGestureRecognizer)?.velocity(in: (sender as AnyObject).view)

        if(sender as? UIPanGestureRecognizer)?.state == .began {
            if isDown {
                self.setTrayConstraintsToDown()
            } else {
                self.setTrayConstraintsToUp()
            }
        }

        if(sender as? UIPanGestureRecognizer)?.state == .ended {
            if (velocity?.y)! > casted {
                print("Gesture went down")
                pullDown()
            } else {
                print("Gesture went up")
                pullUp()
            }
        }

        if (sender as? UIPanGestureRecognizer)?.state == .changed {
            if (velocity?.y)! > casted {
                if isDown {
                    //If we aren't showing the left panel, don't allow scrolling to right
                    return
                }
                //If we are, need to allow to navigate back home
            } else {
                if !isDown {
                    return
                }
            }

            //Are you more than halfway? If so, show the panel when done dragging by setting this value to YES(1).
            //print("First val: \(abs((sender as AnyObject).view.center.y - (self.view.frame.size.height) / 2))")
            //print("Second Val: \((self.view.frame.size.height)/2)")
            //isShowPanel = abs((sender as AnyObject).view.center.y - (self.view.frame.size.height) / 2) < 270

            //Allow dragging only in y-coordinates by only updating the y-coordinate with translation position.
            (sender as AnyObject).view.center = CGPoint(x: CGFloat((sender as AnyObject).view.center.x), y: CGFloat((sender as AnyObject).view.center.y + (translatedPoint?.y)!))
            (sender as? UIPanGestureRecognizer)?.setTranslation(CGPoint(x: CGFloat(0), y: CGFloat(0)), in: view)
        }
    }
    
    func upButtonPressed() {
        if isDown {
            pullUp()
        } else {
            pullDown()
        }
    }
    
}

