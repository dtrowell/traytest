//
//  TrayView.swift
//  slideViewTutorial
//
//  Created by ProObject on 7/20/17.
//  Copyright © 2017 ProObject. All rights reserved.
//

import UIKit

class TrayView: UIView, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var upButton: UIButton!
    
    //MARK: - Info Labels
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!

    @IBOutlet weak var mainInfoTable: UITableView!
    @IBOutlet weak var topInfoStack: UIStackView!
    
    var topInfoDict: [String: String] = ["Runner": "USERNAME", "Distance": "XX.XX mi", "Time": "XX:XX AM"]
    var mainInfoDict: [String: String] = ["Speed": "XX.XX mph", "Current Mile": "XX", "Elasped Time": "XX:XX:XX",
                                            "Latitude": "-XXX.XXXXXX", "Longitude": "-XXX.XXXXXX", "Race Name": "The Biggest Marathon", "Time to Goal": "XX:XX:XX", "Time to Next Mile": "XX:XX"]
    var mainInfoLabels = [String]()
    
    var passedCheckpoints: Int = 6
    
    let dualArrow = "\u{21D5}"
    let cellBackgroundColor: UIColor = UIColor(red: (85.0/255.0), green: (85.0/255.0), blue: (85.0/255.0), alpha: 1.0)
    
    weak var parentView: ViewController?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        //we're going to do stuff here
        Bundle.main.loadNibNamed("TrayView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        //Done initializing the view, now worry about the outlets
        
        upButton.setTitle(dualArrow, for: .normal)
        
        self.mainInfoTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        setTopLabels()
        setMainLabels()
        
        mainInfoTable.reloadData()
    }

    @IBAction func upButtonListener(_ sender: Any) {
        parentView?.upButtonPressed()
    }
    
    func setTopLabels() {
        usernameLabel.text = "\tRunner: \(topInfoDict["Runner"] ?? "USERNAME")"
        timeLabel.text = "\tTime: \(topInfoDict["Time"] ?? "XX:XX:XX AM")"
        distanceLabel.text = "\tDistance: \(topInfoDict["Distance"] ?? "XX.XX mi")"
    }
    
    //MARK: - Table View Functions
    func setMainLabels() {
        mainInfoLabels.append("Speed: \(mainInfoDict["Speed"] ?? "XX.XX mph")")
        mainInfoLabels.append("Current Mile: \(mainInfoDict["Current Mile"] ?? "XX")")
        mainInfoLabels.append("Elapsed Time: \(mainInfoDict["Elapsed Time"] ?? "XX:XX:XX")")
        mainInfoLabels.append("Latitude: \(mainInfoDict["Latitude"] ?? "-XXX.XXXXX")")
        mainInfoLabels.append("Longitude: \(mainInfoDict["Longitude"] ?? "-XXX.XXXXX")")
        mainInfoLabels.append("Race Name: \(mainInfoDict["Race Name"] ?? "The Big Marathon")")
        mainInfoLabels.append("Time to Goal: \(mainInfoDict["Time to Goal"] ?? "XX:XX:XX")")
        mainInfoLabels.append("Time to Next Mile: \(mainInfoDict["Time to Next Mile"] ?? "XX:XX")")
        addPassedCheckpoints()
    }
    
    func addPassedCheckpoints() {
        for i in 1..<passedCheckpoints {
            mainInfoLabels.append("Mile \(i): XX:XX")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainInfoLabels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = self.mainInfoLabels[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        cell.backgroundColor = cellBackgroundColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK: - Which data functions
    func useData(fromPoint: Int) {
        mainInfoLabels = []
        switch (fromPoint) {
        case 1:
            topInfoDict["Runner"] = "Devin"
            topInfoDict["Time"] = "10:47 AM"
            topInfoDict["Distance"] = "2.01 mi"
            break
        case 2:
            topInfoDict["Runner"] = "Devin"
            topInfoDict["Time"] = "10:54 AM"
            topInfoDict["Distance"] = "3.00 mi"
            break
        default:
            break
        }
        
        setTopLabels()
        setMainLabels()
        mainInfoTable.reloadData()
    }
    
}
